import psycopg2
import re

def query(sql, p1, p2):
    return sql % (p1, p2)

def get_plan(sql, p1, p2, database="rydb"):
    conn = psycopg2.connect("dbname=%s user=justin password=postgres" % (database,))

    cur = conn.cursor()
    cur.execute(query(sql, p1, p2))
    plan = "\n".join(map(lambda x: x[0], cur.fetchall()))
    plan = re.sub("\(.*\)", "", plan)

    return plan

def get_cost(sql, p1, p2, database="rydb"):
    conn = psycopg2.connect("dbname=%s user=justin password=postgres" % (database,))

    cur = conn.cursor()
    cur.execute(query(sql, p1, p2))
    plan = "\n".join(map(lambda x: x[0], cur.fetchall()))
    return list(map(float, re.search(r'^.*cost=(\S+)', plan).group(1).split("..")))

# No good PK for lineitem or partsupp.
KEYS = {
        "supplier": "s_suppkey",
        "orders": "o_orderkey",
        "nation": "n_nationkey",
        "region": "r_regionkey",
        "part": "p_partkey",
        "customer": "c_custkey",
        }

CARDS = {
        "supplier": 10000,
        "orders": 1500000,
        "nation": 25,
        "region": 5,
        "part": 200000,
        "customer": 150000,
        }


class TPCHQuery:
    def __init__(self, tables, q):
        self.tables = tables
        self.query = q

    def parameterize(self, t1, t2):
        return self.query % (KEYS[t1], KEYS[t2])

Q2 = TPCHQuery(["part", "supplier", "nation", "region"],
"""
EXPLAIN SELECT
    s_acctbal,
    s_name,
    n_name,
    p_partkey,
    p_mfgr,
    s_address,
    s_phone,
    s_comment
FROM
    part,
    supplier,
    partsupp,
    nation,
    region
WHERE
    p_partkey = ps_partkey
    AND s_suppkey = ps_suppkey
    AND p_size = 15
    AND p_type LIKE '%%%%BRASS'
    AND s_nationkey = n_nationkey
    AND n_regionkey = r_regionkey
    AND r_name = 'EUROPE'
    AND ps_supplycost = (
        SELECT
            min(ps_supplycost)
        FROM
            partsupp,
            supplier,
            nation,
            region
        WHERE
            p_partkey = ps_partkey
            AND s_suppkey = ps_suppkey
            AND s_nationkey = n_nationkey
            AND n_regionkey = r_regionkey
            AND r_name = 'EUROPE'
    )
    AND %s < %%d
    AND %s < %%d
ORDER BY
    s_acctbal DESC,
    n_name,
    s_name,
    p_partkey
LIMIT 100;
""")

Q21 = TPCHQuery(["supplier", "orders", "nation"],
"""
EXPLAIN SELECT
    s_name,
    count(*) AS numwait
FROM
    supplier,
    lineitem l1,
    orders,
    nation
WHERE
    s_suppkey = l1.l_suppkey
    AND o_orderkey = l1.l_orderkey
    AND o_orderstatus = 'F'
    AND l1.l_receiptDATE > l1.l_commitdate
    AND EXISTS (
        SELECT
            *
        FROM
            lineitem l2
        WHERE
            l2.l_orderkey = l1.l_orderkey
            AND l2.l_suppkey <> l1.l_suppkey
    )
    AND NOT EXISTS (
        SELECT
            *
        FROM
            lineitem l3
        WHERE
            l3.l_orderkey = l1.l_orderkey
            AND l3.l_suppkey <> l1.l_suppkey
            AND l3.l_receiptDATE > l3.l_commitdate
    )
    AND s_nationkey = n_nationkey
    AND n_name = 'SAUDI ARABIA'
    AND %s < %%d
    AND %s < %%d
GROUP BY
    s_name
ORDER BY
    numwait DESC,
    s_name
LIMIT 100;
""")
